from datetime import datetime
from pdb import pm
import matplotlib.pyplot as plt
import matplotlib as mpl

# mpl.rcParams['font.family'] = 'sans-serif'
# mpl.rcParams['font.sans-serif'] = ['Arial']
mpl.rcParams['font.family'] = 'Avenir'
mpl.rcParams['text.usetex'] = True
# mpl.rcParams['figure.autolayout'] = True
mpl.rcParams['font.size'] = 20
mpl.rcParams['axes.linewidth'] = 1
mpl.rcParams['lines.markersize'] = 10

import numpy as np

np.set_printoptions(
    edgeitems=4,
    linewidth=130,
    # suppress=True,
    threshold=1000,
    precision=5)
import scipy as sp
import pinocchio as pin
import csv

import sys
import os

srcabspath = os.path.join(os.path.abspath(os.path.dirname(__file__)))
print('abspath(__file__):', srcabspath)

import sys
import os

buildabspath = os.path.join(os.path.abspath(os.path.dirname(__file__)),
                            '../build/')
print('buildabspath:', buildabspath)
sys.path.append(buildabspath)
import lin

ALPHA_PKFILE = os.path.join(srcabspath, 'alpha.pickle')

##########################################################################
# initial conditions and simulation parameters
nd = 6  # position-space size
dt = 1e-3

LINEWIDTH = 3


def savetex_if_notexists(text, texfile):
    if not os.path.exists(texfile):
        print('saving {}...'.format(texfile))
        with open(texfile, 'w') as mf:
            mf.write(text)


pose0 = pin.SE3(np.eye(3), np.zeros(3))

# this specific value is a possible panda configuration
pose0 = pin.SE3(
    # np.array([[0.955337, 2.07405e-06, 0.295517],
    #           [0.166863, -0.825336, -0.539424], [0.2439, 0.564642,
    #                                              -0.788474]]),
    pin.rpy.rotate('x', np.pi / 8),
    np.array([-.15, .1, .2]))
# np.zeros(3))
# np.array([0.55057, 0.014684, 0.752962]))
pose02 = pin.SE3(pin.rpy.rotate('x', np.pi / 3), np.array([.25, -.15, .3]))

#####################################
# ptarget = np.copy(pose0.translation)
# ptarget = np.copy(pose0.translation) + np.array([0, 0, -.2])
# ptarget = np.copy(pose0.translation) + np.array([0, -.3, 0])
# ptarget = np.copy(pose0.translation) + np.array([.2, 0, 0])
ptarget = np.copy(pose0.translation) + np.array([.5, -.3, -.4])
# ptarget = np.copy(pose0.translation) + np.array([0, -.3, -.4])  # drifting
#####################################
rtarget = np.copy(pose0.rotation)
# rtarget = pin.rpy.rotate('x', np.pi / 2)
# rtarget = pin.rpy.rotate('x', np.pi / 3) @ pose0.rotation
rtarget = pin.rpy.rotate('x', np.pi / 6) @ pin.rpy.rotate(
    'z', np.pi / 4) @ pose0.rotation
# rtarget = pin.rpy.rotate('y', np.pi / 4) @ pin.rpy.rotate(
#     'z', np.pi / 6) @ pin.rpy.rotate('y', np.pi / 3) @ pin.rpy.rotate(
#         'x', np.pi / 2) @ pose0.rotation
# rtarget = np.eye(3)
posetarget = pin.SE3(rtarget, ptarget)
posetarget2 = pin.SE3(
    pin.rpy.rotate('x', np.pi / 8) @ pin.rpy.rotate('z', np.pi / 5),
    np.copy(pose0.translation) + np.array([.3, -.25, .4]))
print('pose0:', pose0)
print('posetarget:', posetarget)
#####################################
textemplate = '${{ ||\p_f-\p_0||={:.2f} }}$m and~${{||ang(\delta)||={:.2f}}}$rad'
print('pose0->target:\n', pin.log(pose0.actInv(posetarget)))
print(15 * '-')
print('Trajectory #1: pose0->target')
delta = pin.log(pose0.actInv(posetarget))
deltaang = np.linalg.norm(delta.angular)
deltalin = np.linalg.norm(pin.exp(delta).translation)
savetex_if_notexists(text=textemplate.format(deltalin, deltaang),
                     texfile='traj1.tex')
print(' linear mod: {:.2f} m'.format(deltalin))
print('angular mod: {:.2f} rad'.format(deltaang))
print(15 * '-')
print('\n')
print('pose02:', pose02)
print('posetarget2:', posetarget2)
print(15 * '-')
print('Trajectory #2: pose02->target2')
delta = pin.log(pose02.actInv(posetarget2))
deltaang = np.linalg.norm(delta.angular)
deltalin = np.linalg.norm(pin.exp(delta).translation)
savetex_if_notexists(text=textemplate.format(deltalin, deltaang),
                     texfile='traj2.tex')
print(' linear mod: {:.2f} m'.format(deltalin))
print('angular mod: {:.2f} rad'.format(deltaang))
print(15 * '-')
print('\n')

################################################################
# trajectory generation max
tt = 0
ref_t, ref_se3 = [tt], [pin.log(pose0)]
ref_se32 = [pin.log(pose02)]  # 2nd trajecotory
ref_se3d2 = []
ref_se3d, ref_se3dd = [], []
import pickle as cPickle

pkfile = open(ALPHA_PKFILE, 'rb')
ref_alpha, ref_alphad, ref_alphadd = cPickle.load(pkfile)
for alpha in ref_alpha:
    ref_se3.append(pin.log(pin.SE3.Interpolate(pose0, posetarget, alpha)))
    ref_se32.append(pin.log(pin.SE3.Interpolate(pose02, posetarget2, alpha)))
    tt = tt + dt
    ref_t.append(tt)


def call_cppwrap(func, *args):
    restime = func(*[arg.vector for arg in args])
    compute_time = restime.total_ns / 1e9
    return compute_time, pin.Motion(restime.result)


def savetabular_if_notexists(tabular_rows):
    csvfilepath = 'latex_table.tabular'
    if not os.path.exists(csvfilepath):
        print('saving {}...'.format(csvfilepath))
        csvfile = open(csvfilepath, 'w')
        for row in tabular_rows:
            csvfile.write(row)
        csvfile.close()


def savefig_if_notexists(fig, fpath):
    if not os.path.exists(fpath):
        print('saving {}...'.format(fpath))
        fig.savefig(
            fpath,
            dpi=300,
            bbox_inches="tight",
        )


def lin_from(xlist):
    return [x.linear for x in xlist]


def ang_from(xlist):
    return [x.angular for x in xlist]


def norm_from_vechist(vechist):
    '''
    turns list of pin.Motion into a numpy vector of norms
    '''
    return np.array([np.linalg.norm(v) for v in vechist])


def vec_from_motlist(vec, row, ncut=None):
    '''
    turns list of pin.Motion into a numpy vector
    '''
    if ncut is None:
        ncut = len(vec)
    return np.array([v.vector[row] for v in vec[:ncut]])


def se3err(x1, x2):
    '''
    NOTE: not a real se3 result, linear comp contains translation
    '''
    return pin.Motion(
        np.hstack([
            pin.exp(x1).translation - pin.exp(x2).translation,
            x1.angular - x2.angular
        ]))


def se3skew(se3):
    '''
    se3 skew for matricial multiplication
    '''
    return np.vstack([
        np.hstack([pin.skew(se3.angular),
                   pin.skew(se3.linear)]),
        np.hstack([np.zeros((3, 3)), pin.skew(se3.angular)])
    ])


def brse3(x1, x2):
    '''
    se3 bracket
    '''
    br = pin.Motion.Zero()
    br.linear = pin.skew(x1.angular) @ x2.linear - pin.skew(
        x2.angular) @ x1.linear
    br.angular = pin.skew(x1.angular) @ x2.angular
    return br


def dexp(x0):
    '''
    Computes the linearization matrix at some point x0
    '''
    br = se3skew(x0)
    return (np.eye(6)  # n=0
            + br / 2  # n=1
            + br @ br / 12  # n=2
            )


def me2(x0, x, dx):
    '''
    Using the M2 linearization at some point x0, compute the value
    of applying an increment dx to x.
    '''
    return x + pin.Motion(dexp(x0) @ dx)


def mot2np_lin(motionlist):
    return np.array([x.vector for x in motionlist], dtype=object)[:, :3]


def mot2np_ang(motionlist):
    return np.array([x.vector for x in motionlist], dtype=object)[:, 3:]


##################################################################
##################################################################
##################################################################
# HORIZON SIMULATION
class HorizonStats:
    def __init__(self):
        self.err_lin_avg_mm = {}
        self.err_lin_std_mm = {}
        self.err_lin_max_mm = {}
        self.err_ang_avg_rad = {}
        self.err_ang_std_rad = {}
        self.err_ang_max_rad = {}
        self.horizon_steps = None
        self.horizon_duration_s = None
        self.horizon_deltat_s = None


class HorizonParam:
    def __init__(self, ndt, steps):
        self.ndt = ndt
        self.steps = steps


ndt = 1
nmax = len(ref_t) - 2
horizon_stats = []
for horizon in [
        HorizonParam(ndt=1, steps=steps) for steps in (np.arange(100) + 1) * 10
]:
    horizonstat = HorizonStats()
    hdt = horizon.ndt * dt
    horizonstat.horizon_deltat_s = hdt
    horizonstat.horizon_duration_s = hdt * horizon.steps
    horizonstat.horizon_steps = horizon.steps

    print('simulating H={}...'.format(horizon.steps))

    if horizonstat.horizon_duration_s > tt:
        print(
            '{}s>{}s, horizon is bigger than trajectory, ending simulation...'.
            format(horizonstat.horizon_duration_s, tt))
        break

    ##############################################################

    ##################################################################
    hist_pose_exp = [ref_se3[0]]
    hist_pose_me2 = [ref_se3[0]]
    hist_err_me2 = [pin.Motion.Zero()]

    # second trajectory
    hist_pose_exp2 = [ref_se32[0]]
    hist_pose_me22 = [ref_se32[0]]
    hist_err_me22 = [pin.Motion.Zero()]

    hist_compute_times = [np.zeros(4)]

    tvec = [0]
    tsyncs = [0]  # t where sync happened
    next_sync = 0
    pose0_horizon = ref_se3[0]
    pose0_horizon = ref_se32[0]
    for k in ndt * (np.arange(nmax) + 1):
        refn = k
        refnext = refn + ndt
        refnnext = refnext + ndt
        if refnnext > nmax:
            refnext = nmax
            # print(
            #     'refn:{}, refnext:{}, refnnext:{}, len(tvec):{}, t:{}'.format(
            #         refn, refnext, refnnext, len(tvec), t))
            break

        t = k * dt
        tvec.append(t)

        # compute desired
        des_pose2 = ref_se32[refn]
        des_pose_next2 = ref_se32[refnext]
        des_posed2 = pin.log(
            pin.exp(des_pose2).actInv(pin.exp(des_pose_next2))) / hdt

        des_pose = ref_se3[refn]
        des_pose_next = ref_se3[refnext]
        des_pose_nnext = ref_se3[refnnext]
        des_posed = pin.log(pin.exp(des_pose).actInv(
            pin.exp(des_pose_next))) / hdt
        des_posed_next = pin.log(
            pin.exp(des_pose_next).actInv(pin.exp(des_pose_nnext))) / hdt
        des_posedd = (des_posed_next - des_posed) / hdt

        compute_times = np.zeros(4)

        if t >= next_sync:
            posek_me2 = posek_exp = des_pose_next
            posek_me22 = posek_exp2 = des_pose_next2
            pose0_horizon = des_pose_next
            pose0_horizon2 = des_pose_next2

            dexp0 = dexp(x0=pose0_horizon)
            dexp02 = dexp(x0=pose0_horizon2)

            if next_sync != 0:
                tsyncs.append(t)
            next_sync = t + horizon.steps * hdt
        else:

            computetime_exp, posek_exp = call_cppwrap(lin.pinlog_timed,
                                                      hist_pose_exp[-1],
                                                      hdt * des_posed)
            computetime_exp2, posek_exp2 = call_cppwrap(
                lin.pinlog_timed, hist_pose_exp2[-1], hdt * des_posed2)

            computetime_me2, posek_me2 = call_cppwrap(lin.me2_timed,
                                                      pose0_horizon,
                                                      hist_pose_me2[-1],
                                                      hdt * des_posed)
            computetime_me22, posek_me22 = call_cppwrap(
                lin.me2_timed, pose0_horizon2, hist_pose_me22[-1],
                hdt * des_posed2)

            compute_times[0] = computetime_exp
            compute_times[1] = computetime_me2
            compute_times[2] = computetime_exp2
            compute_times[3] = computetime_me22

        def so3lim(mot):
            if np.linalg.norm(mot.angular) >= np.pi:
                mot.angular = -mot.angular

        # traj 2
        so3lim(posek_exp2)
        so3lim(posek_me22)
        hist_pose_me22.append(posek_me22)
        hist_pose_exp2.append(posek_exp2)
        hist_err_me22.append(se3err(des_pose_next2, posek_me22))
        ref_se3d2.append(des_posed2)

        # traj 1
        so3lim(posek_exp)
        so3lim(posek_me2)
        hist_err_me2.append(se3err(des_pose_next, posek_me2))
        hist_pose_me2.append(posek_me2)
        hist_pose_exp.append(posek_exp)

        ref_se3d.append(des_posed)
        ref_se3dd.append(des_posedd)

        hist_compute_times.append(compute_times)

    tnp = np.array(tvec)
    tsyncs.append(t)

    ############################################################################
    # COMPUTATION TIMES and ERROR TABLE
    labels = ['traj-1', 'traj-2']
    hist_errs = [hist_err_me2, hist_err_me22]
    for klabel, label in list(enumerate(labels)):
        # err stats
        label_errnp_linnorm = norm_from_vechist(mot2np_lin(
            hist_errs[klabel])) * 1e3  # [m] to [mm]
        horizonstat.err_lin_avg_mm[label] = np.average(label_errnp_linnorm)
        horizonstat.err_lin_std_mm[label] = np.std(label_errnp_linnorm)
        horizonstat.err_lin_max_mm[label] = np.max(label_errnp_linnorm)

        label_errnp_angnorm = norm_from_vechist(mot2np_ang(hist_errs[klabel]))
        horizonstat.err_ang_avg_rad[label] = np.average(label_errnp_angnorm)
        horizonstat.err_ang_std_rad[label] = np.std(label_errnp_angnorm)
        horizonstat.err_ang_max_rad[label] = np.max(label_errnp_angnorm)

    horizon_stats.append(horizonstat)

############################################################################
############################################################################
############################################################################
############################################################################


def configure_figure(ax,
                     fig,
                     ylabel,
                     title=None,
                     xlabel='time [s]',
                     savefig=None,
                     legend_loc='upper left'):
    ax.legend(
        shadow=True,
        fancybox=True,
        loc=legend_loc,
        frameon=False,
        # bbox_to_anchor=(1, 1.05)
    )

    leg = ax.legend()
    for lh in leg.legendHandles:
        lh.set_alpha(1)

    ax.set_ylabel(
        ylabel,
        rotation='vertical',
        labelpad=labelpad,
    )
    ax.set_xlabel(xlabel)
    ax.ticklabel_format(axis='y', style='sci', scilimits=(0, 0))

    if savefig is not None:
        savefig_if_notexists(fig=fig, fpath=savefig + '.png')

    if title is not None:
        fig.suptitle(title)


markers = ['o', 'X']
labelpad = 12

############################################################################
## lin vs steps
figlin, axeslin = plt.subplots(1, 1, sharex='col')
figang, axesang = plt.subplots(1, 1, sharex='col')
for axis in [axeslin, axesang]:
    axis.margins(x=-0.02, y=-0.05)

    axis.spines['right'].set_visible(False)
    axis.spines['top'].set_visible(False)
    # axis.spines['bottom'].set_visible(False)
    axis.yaxis.set_tick_params(
        which='major',
        size=5,
        width=1,
        direction='out',
    )
    axis.get_yaxis().set_major_locator(mpl.ticker.LinearLocator(numticks=5))
    axis.get_xaxis().set_major_locator(mpl.ticker.LinearLocator(numticks=5))

alpha = 0.3
subsample = 3
for label, marker in zip(labels, markers):
    print('plotting', label)
    x, y, z = [], [], []
    y_max, z_max = [], []
    y_std, z_std = [], []
    for hor in horizon_stats:
        x.append(hor.horizon_steps)
        y.append(hor.err_lin_avg_mm[label])
        y_max.append(hor.err_lin_max_mm[label])
        y_std.append(hor.err_lin_std_mm[label])
        z.append(hor.err_ang_avg_rad[label])
        z_max.append(hor.err_ang_max_rad[label])
        z_std.append(hor.err_ang_std_rad[label])
    x, y, z, y_max, y_std, z_max, z_std = np.array(x), np.array(y), np.array(
        z), np.array(y_max), np.array(y_std), np.array(z_max), np.array(z_std)
    ax = axeslin
    ax.plot(
        x[::subsample],
        y[::subsample],
        marker=marker,
        alpha=alpha,
        color='black',
        linewidth=LINEWIDTH,
        clip_on=False,
    )
    ax.scatter(
        x[::subsample],
        y[::subsample],
        label=label,
        marker=marker,
        alpha=alpha,
        color='black',
        clip_on=False,
    )
    ax.set_ylim(bottom=0, top=np.max(y))
    ax.set_xlim(left=0, right=np.max(x))
    # ax.fill_between(x, y - y_max, y + y_max, alpha=0.2)
    # ax.fill_between(x, y - y_std, y + y_std, alpha=0.45)
    configure_figure(
        ax=ax,
        fig=figlin,
        # title='lin err vs H-steps',
        ylabel='[mm]',
        xlabel='horizon length in steps of 1ms',
        savefig='multihorizon_lin_err')

    ax = axesang
    ax.plot(
        x[::subsample],
        z[::subsample],
        marker=marker,
        alpha=alpha,
        color='black',
        linewidth=LINEWIDTH,
        clip_on=False,
    )
    ax.scatter(
        x[::subsample],
        z[::subsample],
        label=label,
        marker=marker,
        alpha=alpha,
        color='black',
        clip_on=False,
    )
    ax.set_ylim(bottom=0, top=np.max(z))
    ax.set_xlim(left=0, right=np.max(x))
    # ax.fill_between(x, z - z_max, z + z_max, alpha=0.2)
    # ax.fill_between(x, z - z_std, z + z_std, alpha=0.45)
    configure_figure(
        ax=ax,
        fig=figang,
        # title='ang err vs H-steps',
        ylabel='[rad]',
        xlabel='steps of 1ms',
        savefig='multihorizon_ang_err')

plt.show()
