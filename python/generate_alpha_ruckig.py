import matplotlib.pyplot as plt
import matplotlib as mpl
import pickle

# mpl.rcParams['font.family'] = 'sans-serif'
# mpl.rcParams['font.sans-serif'] = ['Arial']
mpl.rcParams['font.family'] = 'Avenir'
mpl.rcParams['text.usetex'] = True
# mpl.rcParams['figure.autolayout'] = True
mpl.rcParams['font.size'] = 20
mpl.rcParams['axes.linewidth'] = 1
mpl.rcParams['lines.markersize'] = 15

import numpy as np
import ruckig as ruc

np.set_printoptions(
    edgeitems=4,
    linewidth=130,
    # suppress=True,
    threshold=1000,
    precision=5)
import scipy as sp

import sys

##########################################################################
# initial conditions and simulation parameters
nd = 6  # position-space size
dt = 1e-3

LINEWIDTH = 3

################################################################
# trajectory generation max
ruckig = ruc.Ruckig(dofs=1, delta_time=dt)
inp = ruc.InputParameter(dofs=1)
out = ruc.OutputParameter(dofs=1)

inp.max_velocity = [1.5]
inp.max_acceleration = [4]
inp.max_jerk = [500]

inp.current_position = [0]
inp.current_velocity = [0]
inp.current_acceleration = [0]

inp.target_velocity = [0]
inp.target_acceleration = [0]
inp.target_position = [1]

tt = 0
ref_t = [tt]
ref_alpha, ref_alphad, ref_alphadd = [inp.current_position[0]
                                      ], [inp.current_velocity[0]
                                          ], [inp.current_acceleration[0]]
result = ruckig.update(inp, out)
while result == ruc.Working:
    inp.current_position = out.new_position
    inp.current_velocity = out.new_velocity
    inp.current_acceleration = out.new_acceleration

    tt = tt + dt
    ref_t.append(tt)
    ref_alpha.append(out.new_position[-1])
    ref_alphad.append(out.new_velocity[-1])
    ref_alphadd.append(out.new_acceleration[-1])

    result = ruckig.update(inp, out)

if result != ruc.Finished:
    print('Error: Ruckig result:', result)
    sys.exit(-1)

#################################################################
#################################################################
# alpha profile graph
fontsize = 15
labelpad = 5
toplot = [ref_alpha, ref_alphad, ref_alphadd]
labels = [
    r'$\alpha$',
    r'$\dot{\alpha}$',
    r'$\ddot{\alpha}$',
]
for k in np.arange(3):
    fig, ax = plt.subplots(1, 1, sharex='col')
    ax.plot(ref_t,
            toplot[k],
            label='alpha' + 'd' * k,
            color='black',
            linewidth=LINEWIDTH)
    ax.set_ylabel(labels[k],
                  labelpad=labelpad,
                  rotation='vertical',
                  fontsize=fontsize)
    ax.set_xlabel('time [s]')
    # ax.spines['left'].set_visible(False)
    # ax.spines['bottom'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)

    name = 'alpha' + 'd' * k + '.png'
    # fig.savefig(name, dpi=300, bbox_inches='tight')
    # plt.close(fig)

#################################################################
#################################################################

# plt.show()

with open('alpha.pickle', 'wb') as fh:
    pickle.dump([ref_alpha, ref_alphad, ref_alphadd], fh)
