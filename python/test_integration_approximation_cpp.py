from datetime import datetime
from pdb import pm
import matplotlib.pyplot as plt
import matplotlib as mpl

# mpl.rcParams['font.family'] = 'sans-serif'
# mpl.rcParams['font.sans-serif'] = ['Arial']
mpl.rcParams['font.family'] = 'Avenir'
mpl.rcParams['text.usetex'] = True
# mpl.rcParams['figure.autolayout'] = True
mpl.rcParams['font.size'] = 20
mpl.rcParams['axes.linewidth'] = 1
mpl.rcParams['lines.markersize'] = 10

import numpy as np

np.set_printoptions(
    edgeitems=4,
    linewidth=130,
    # suppress=True,
    threshold=1000,
    precision=5)
import scipy as sp
import pinocchio as pin
import csv

import sys
import os

srcabspath = os.path.join(os.path.abspath(os.path.dirname(__file__)))
print('abspath(__file__):', srcabspath)

import sys
import os

buildabspath = os.path.join(os.path.abspath(os.path.dirname(__file__)),
                            '../build/')
print('buildabspath:', buildabspath)
sys.path.append(buildabspath)
import lin

ALPHA_PKFILE = os.path.join(srcabspath, 'alpha.pickle')

##########################################################################
# initial conditions and simulation parameters
nd = 6  # position-space size
dt = 1e-3

LINEWIDTH = 3


def savetex_if_notexists(text, texfile):
    if not os.path.exists(texfile):
        print('saving {}...'.format(texfile))
        with open(texfile, 'w') as mf:
            mf.write(text)


pose0 = pin.SE3(np.eye(3), np.zeros(3))

# this specific value is a possible panda configuration
pose0 = pin.SE3(
    # np.array([[0.955337, 2.07405e-06, 0.295517],
    #           [0.166863, -0.825336, -0.539424], [0.2439, 0.564642,
    #                                              -0.788474]]),
    pin.rpy.rotate('x', np.pi / 8),
    np.array([-.15, .1, .2]))
# np.zeros(3))
# np.array([0.55057, 0.014684, 0.752962]))
pose02 = pin.SE3(pin.rpy.rotate('x', np.pi / 3), np.array([.25, -.15, .3]))

#####################################
# ptarget = np.copy(pose0.translation)
# ptarget = np.copy(pose0.translation) + np.array([0, 0, -.2])
# ptarget = np.copy(pose0.translation) + np.array([0, -.3, 0])
# ptarget = np.copy(pose0.translation) + np.array([.2, 0, 0])
ptarget = np.copy(pose0.translation) + np.array([.5, -.3, -.4])
# ptarget = np.copy(pose0.translation) + np.array([0, -.3, -.4])  # drifting
#####################################
rtarget = np.copy(pose0.rotation)
# rtarget = pin.rpy.rotate('x', np.pi / 2)
# rtarget = pin.rpy.rotate('x', np.pi / 3) @ pose0.rotation
rtarget = pin.rpy.rotate('x', np.pi / 6) @ pin.rpy.rotate(
    'z', np.pi / 4) @ pose0.rotation
# rtarget = pin.rpy.rotate('y', np.pi / 4) @ pin.rpy.rotate(
#     'z', np.pi / 6) @ pin.rpy.rotate('y', np.pi / 3) @ pin.rpy.rotate(
#         'x', np.pi / 2) @ pose0.rotation
# rtarget = np.eye(3)
posetarget = pin.SE3(rtarget, ptarget)
posetarget2 = pin.SE3(
    pin.rpy.rotate('x', np.pi / 8) @ pin.rpy.rotate('z', np.pi / 5),
    np.copy(pose0.translation) + np.array([.3, -.25, .4]))
print('pose0:', pose0)
print('posetarget:', posetarget)
#####################################
textemplate = '${{ ||\p_f-\p_0||={:.2f} }}$m and~${{||ang(\delta)||={:.2f}}}$rad'
print('pose0->target:\n', pin.log(pose0.actInv(posetarget)))
print(15 * '-')
print('Trajectory #1: pose0->target')
delta = pin.log(pose0.actInv(posetarget))
deltaang = np.linalg.norm(delta.angular)
deltalin = np.linalg.norm(pin.exp(delta).translation)
savetex_if_notexists(text=textemplate.format(deltalin, deltaang),
                     texfile='traj1.tex')
print(' linear mod: {:.2f} m'.format(deltalin))
print('angular mod: {:.2f} rad'.format(deltaang))
print(15 * '-')
print('\n')
print('pose02:', pose02)
print('posetarget2:', posetarget2)
print(15 * '-')
print('Trajectory #2: pose02->target2')
delta = pin.log(pose02.actInv(posetarget2))
deltaang = np.linalg.norm(delta.angular)
deltalin = np.linalg.norm(pin.exp(delta).translation)
savetex_if_notexists(text=textemplate.format(deltalin, deltaang),
                     texfile='traj2.tex')
print(' linear mod: {:.2f} m'.format(deltalin))
print('angular mod: {:.2f} rad'.format(deltaang))
print(15 * '-')
print('\n')

################################################################
# trajectory generation max
tt = 0
ref_t, ref_se3 = [tt], [pin.log(pose0)]
ref_se32 = [pin.log(pose02)]  # 2nd trajecotory
ref_se3d2 = []
ref_se3d, ref_se3dd = [], []
import pickle as cPickle

pkfile = open(ALPHA_PKFILE, 'rb')
ref_alpha, ref_alphad, ref_alphadd = cPickle.load(pkfile)
for alpha in ref_alpha:
    ref_se3.append(pin.log(pin.SE3.Interpolate(pose0, posetarget, alpha)))
    ref_se32.append(pin.log(pin.SE3.Interpolate(pose02, posetarget2, alpha)))
    tt = tt + dt
    ref_t.append(tt)


def call_cppwrap(func, *args):
    restime = func(*[arg.vector for arg in args])
    compute_time = restime.total_ns / 1e9
    return compute_time, pin.Motion(restime.result)


def savetabular_if_notexists(tabular_rows):
    csvfilepath = 'latex_table.tabular'
    if not os.path.exists(csvfilepath):
        print('saving {}...'.format(csvfilepath))
        csvfile = open(csvfilepath, 'w')
        for row in tabular_rows:
            csvfile.write(row)
        csvfile.close()


def savefig_if_notexists(fig, fpath):
    if not os.path.exists(fpath):
        print('saving {}...'.format(fpath))
        fig.savefig(
            fpath,
            dpi=300,
            bbox_inches="tight",
        )


def lin_from(xlist):
    return [x.linear for x in xlist]


def ang_from(xlist):
    return [x.angular for x in xlist]


def norm_from_vechist(vechist):
    '''
    turns list of pin.Motion into a numpy vector of norms
    '''
    return np.array([np.linalg.norm(v) for v in vechist])


def vec_from_motlist(vec, row, ncut=None):
    '''
    turns list of pin.Motion into a numpy vector
    '''
    if ncut is None:
        ncut = len(vec)
    return np.array([v.vector[row] for v in vec[:ncut]])


def se3err(x1, x2):
    '''
    NOTE: not a real se3 result, linear comp contains translation
    '''
    return pin.Motion(
        np.hstack([
            pin.exp(x1).translation - pin.exp(x2).translation,
            x1.angular - x2.angular
        ]))


def se3skew(se3):
    '''
    se3 skew for matricial multiplication
    '''
    return np.vstack([
        np.hstack([pin.skew(se3.angular),
                   pin.skew(se3.linear)]),
        np.hstack([np.zeros((3, 3)), pin.skew(se3.angular)])
    ])


def brse3(x1, x2):
    '''
    se3 bracket
    '''
    br = pin.Motion.Zero()
    br.linear = pin.skew(x1.angular) @ x2.linear - pin.skew(
        x2.angular) @ x1.linear
    br.angular = pin.skew(x1.angular) @ x2.angular
    return br


def dexp(x0):
    '''
    Computes the linearization matrix at some point x0
    '''
    br = se3skew(x0)
    return (np.eye(6)  # n=0
            + br / 2  # n=1
            + br @ br / 12  # n=2
            )


def me2(x0, x, dx):
    '''
    Using the M2 linearization at some point x0, compute the value
    of applying an increment dx to x.
    '''
    return x + pin.Motion(dexp(x0) @ dx)


##################################################################
hist_pose_exp = [ref_se3[0]]
hist_pose_me2 = [ref_se3[0]]
hist_err_me2 = [pin.Motion.Zero()]

# second trajectory
hist_pose_exp2 = [ref_se32[0]]
hist_pose_me22 = [ref_se32[0]]
hist_err_me22 = [pin.Motion.Zero()]

hist_compute_times = [np.zeros(4)]

# ndt = 15
# horizon_H = 20
ndt = 1
horizon_H = 300
nmax = len(ref_t) - 2
tvec = [0]
tsyncs = [0]  # t where sync happened
next_sync = 0
pose0_horizon = ref_se3[0]
pose0_horizon = ref_se32[0]
for k in ndt * (np.arange(nmax) + 1):
    refn = k
    refnext = refn + ndt
    refnnext = refnext + ndt
    if refnnext > nmax:
        refnext = nmax
        print('refn:{}, refnext:{}, refnnext:{}, len(tvec):{}, t:{}'.format(
            refn, refnext, refnnext, len(tvec), t))
        break

    hdt = ndt * dt
    t = k * dt
    tvec.append(t)

    # compute desired
    des_pose2 = ref_se32[refn]
    des_pose_next2 = ref_se32[refnext]
    des_posed2 = pin.log(pin.exp(des_pose2).actInv(
        pin.exp(des_pose_next2))) / hdt

    des_pose = ref_se3[refn]
    des_pose_next = ref_se3[refnext]
    des_pose_nnext = ref_se3[refnnext]
    des_posed = pin.log(pin.exp(des_pose).actInv(pin.exp(des_pose_next))) / hdt
    des_posed_next = pin.log(
        pin.exp(des_pose_next).actInv(pin.exp(des_pose_nnext))) / hdt
    des_posedd = (des_posed_next - des_posed) / hdt

    compute_times = np.zeros(4)

    if t >= next_sync:
        posek_me2 = posek_exp = des_pose_next
        posek_me22 = posek_exp2 = des_pose_next2
        pose0_horizon = des_pose_next
        pose0_horizon2 = des_pose_next2

        dexp0 = dexp(x0=pose0_horizon)
        dexp02 = dexp(x0=pose0_horizon2)

        if next_sync != 0:
            tsyncs.append(t)
        next_sync = t + horizon_H * hdt
        print('sync t:{:.2f}s, next_sync:{:.2f}s'.format(t, next_sync))
    else:

        computetime_exp, posek_exp = call_cppwrap(lin.pinlog_timed,
                                                  hist_pose_exp[-1],
                                                  hdt * des_posed)
        computetime_exp2, posek_exp2 = call_cppwrap(lin.pinlog_timed,
                                                    hist_pose_exp2[-1],
                                                    hdt * des_posed2)

        computetime_me2, posek_me2 = call_cppwrap(lin.me2_timed, pose0_horizon,
                                                  hist_pose_me2[-1],
                                                  hdt * des_posed)
        computetime_me22, posek_me22 = call_cppwrap(lin.me2_timed,
                                                    pose0_horizon2,
                                                    hist_pose_me22[-1],
                                                    hdt * des_posed2)

        compute_times[0] = computetime_exp
        compute_times[1] = computetime_me2
        compute_times[2] = computetime_exp2
        compute_times[3] = computetime_me22

    def so3lim(mot):
        if np.linalg.norm(mot.angular) >= np.pi:
            mot.angular = -mot.angular

    # traj 2
    so3lim(posek_exp2)
    so3lim(posek_me22)
    hist_pose_me22.append(posek_me22)
    hist_pose_exp2.append(posek_exp2)
    hist_err_me22.append(se3err(des_pose_next2, posek_me22))
    ref_se3d2.append(des_posed2)

    # traj 1
    so3lim(posek_exp)
    so3lim(posek_me2)
    hist_err_me2.append(se3err(des_pose_next, posek_me2))
    hist_pose_me2.append(posek_me2)
    hist_pose_exp.append(posek_exp)

    ref_se3d.append(des_posed)
    ref_se3dd.append(des_posedd)

    hist_compute_times.append(compute_times)

tnp = np.array(tvec)
tsyncs.append(t)


##################################################################
##################################################################
##################################################################
def scatter_these(axis,
                  t,
                  vectors,
                  labels,
                  markers,
                  markers_size,
                  idx_noskip=[],
                  subsample_rate=1,
                  displacement=0,
                  alpha=.5):

    idx_subsampled = np.arange(0, len(tnp), subsample_rate)
    max_idx = len(tnp) - 1  # max index possible
    idx_tsync = np.append(idx_noskip,
                          idx_noskip - 1)  # add point before horizon
    idx_tsync[idx_tsync < 0] = 0

    def displace():
        '''
        this is only used to avoid superposition of markers when downsampling for show
        the 'displacement' refers to using samples next to the one requested in index
        '''
        displace.val += displacement
        if displace.val < 0:
            displace.val = 0
        return displace.val

    displace.val = -1

    mmax = None
    for vector, label, marker, marker_size in zip(vectors, labels, markers,
                                                  markers_size):
        idx_displaced = np.copy(idx_subsampled + displace())
        # correct max valid index after displacement
        idx_displaced[idx_displaced > max_idx] = max_idx
        idx_wnoskips = np.unique(np.append(idx_displaced, idx_tsync))
        idx_hor = idx_wnoskips
        axis.plot(
            t[idx_hor],
            vector[idx_hor],
            marker=marker,
            alpha=alpha,
            color='black',
            # linewidth=.1,
            linewidth=LINEWIDTH,
            clip_on=False,
        )
        # make markers more opaque
        axis.scatter(
            t[idx_hor],
            vector[idx_hor],
            label=label,
            marker=marker,
            # s=marker_size,
            alpha=alpha - 0.2,
            color='black',
            clip_on=False,
        )
        aux = np.max(vector[idx_hor])
        if mmax is None or aux > mmax:
            mmax = aux
    axis.set_ylim(bottom=0, top=mmax)
    axis.set_xlim(left=0, right=np.max(t[idx_hor]))
    axis.margins(x=-0.02, y=-0.05)

    axis.spines['right'].set_visible(False)
    axis.spines['top'].set_visible(False)
    axis.yaxis.set_tick_params(
        which='major',
        size=5,
        width=1,
        direction='out',
    )
    axis.get_yaxis().set_major_locator(mpl.ticker.LinearLocator(numticks=5))
    axis.get_xaxis().set_major_locator(mpl.ticker.LinearLocator(numticks=5))


def plot_cart(figno, title, baseunit, col_offset=0):
    columns = 3
    rows = 3
    fig, ax = plt.subplots(rows, columns, num=figno, sharex='col')
    alpha = 0.5
    color_traj1 = 'blue'
    color_traj2 = 'red'
    color_ref_traj1 = 'darkblue'
    color_ref_traj2 = 'darkred'
    label_traj1 = 'traj-1'
    label_traj2 = 'traj-2'
    lw = 3

    def myplot(ax, vector, lbl, ls, clr, alp):
        ax.plot(ref_t[:vector.shape[0]],
                vector,
                label=lbl,
                linestyle=ls,
                linewidth=lw,
                color=clr,
                alpha=alp)
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)

    fig.suptitle(title)
    for col in np.arange(columns):
        axis = ax[0, col]
        idx = col + col_offset
        if col == 0:
            axis.set_ylabel('[{}]'.format(baseunit))
        axis.set_title(coords[col])
        myplot(ax=axis,
               vector=vec_from_motlist(ref_se3, idx),
               lbl='ref',
               ls='dashed',
               clr=color_ref_traj1,
               alp=alpha)
        myplot(ax=axis,
               vector=vec_from_motlist(ref_se32, idx),
               lbl='ref2',
               ls='dashed',
               clr=color_ref_traj2,
               alp=alpha)

        myplot(ax=axis,
               vector=vec_from_motlist(hist_pose_me2, idx),
               lbl=label_traj1,
               ls='solid',
               clr=color_traj1,
               alp=alpha)
        myplot(ax=axis,
               vector=vec_from_motlist(hist_pose_me22, idx),
               lbl=label_traj2,
               ls='solid',
               clr=color_traj2,
               alp=alpha)
        axis = ax[1, col]
        if col == 0:
            axis.set_ylabel('[{}]'.format(baseunit))
        axis.set_title('err' + coords[col])
        axis.ticklabel_format(axis='y', style='sci', scilimits=(0, 0))
        myplot(ax=axis,
               vector=vec_from_motlist([pin.Motion.Zero() for x in ref_se3],
                                       col),
               lbl='ref',
               clr='black',
               ls='dashed',
               alp=alpha)

        myplot(ax=axis,
               vector=vec_from_motlist(hist_err_me2, idx),
               lbl=label_traj1,
               ls='solid',
               clr=color_traj1,
               alp=alpha)
        myplot(ax=axis,
               vector=vec_from_motlist(hist_err_me22, idx),
               lbl=label_traj2,
               ls='solid',
               clr=color_traj2,
               alp=alpha)
        axis = ax[2, col]
        if col == 0:
            axis.set_ylabel('[{}/s]'.format(baseunit))
        axis.set_title(r'$\dot{{{coord}}}$'.format(coord=coords[col]))
        axis.ticklabel_format(axis='y', style='sci', scilimits=(0, 0))
        myplot(ax=axis,
               vector=vec_from_motlist(ref_se3d, idx),
               lbl='ref',
               ls='dashed',
               clr=color_ref_traj1,
               alp=alpha)

        myplot(ax=axis,
               vector=vec_from_motlist(ref_se3d2, idx),
               lbl='ref2',
               ls='dashed',
               clr=color_ref_traj2,
               alp=alpha)
        axis.set_xlabel('[s]')

    ax.flat[columns - 1].legend(shadow=True,
                                fancybox=True,
                                frameon=False,
                                loc="upper left",
                                bbox_to_anchor=(1, 1.05))


coords = ['x', 'y', 'z']
linestyles = ['solid', 'solid', 'solid']

plot_cart(figno=0, title='position', baseunit='m', col_offset=0)
plot_cart(figno=1, title='angular', baseunit='rad', col_offset=3)

##################################################################
##################################################################
##################################################################
# final paper graphs

fig, ax = plt.subplots(1, 1, sharex='col')
labels = ['traj-1', 'traj-2']
markers = ['o', 'X']
markers_size = [500, 200]
tsyncs_idx = np.in1d(tnp, tsyncs).nonzero()[0]
labelpad = 15
subsample_rate = 25
displacement = 3
alpha = 0.3
scatter_these(
    axis=ax,
    t=tnp,
    idx_noskip=tsyncs_idx,
    subsample_rate=subsample_rate,
    displacement=displacement,
    vectors=[
        norm_from_vechist(lin_from(hist_err_me2)) * 1e3,  # [m] to [mm]
        norm_from_vechist(lin_from(hist_err_me22)) * 1e3,  # [m] to [mm]
    ],
    labels=labels,
    markers=markers,
    markers_size=markers_size,
    alpha=alpha)

ax.legend(
    shadow=True,
    fancybox=True,
    frameon=False,
    loc='upper right',
)
leg = ax.legend()
for lh in leg.legendHandles:
    lh.set_alpha(1)
ax.set_ylabel(
    '[mm]',
    rotation='vertical',
    labelpad=labelpad,
)
ax.set_xlabel('time [s]')
ax.ticklabel_format(axis='y', style='sci', scilimits=(0, 0))
savefig_if_notexists(fig=fig, fpath='integration_approx_linear.png')

fig, ax = plt.subplots(1, 1, sharex='col')
scatter_these(axis=ax,
              t=tnp,
              idx_noskip=tsyncs_idx,
              subsample_rate=subsample_rate,
              displacement=displacement,
              vectors=[
                  norm_from_vechist(ang_from(hist_err_me2)),
                  norm_from_vechist(ang_from(hist_err_me22)),
              ],
              labels=labels,
              markers=markers,
              markers_size=markers_size,
              alpha=alpha)

ax.legend(
    shadow=True,
    fancybox=True,
    frameon=False,
    loc='upper right',
)
leg = ax.legend()
for lh in leg.legendHandles:
    lh.set_alpha(1)
ax.set_ylabel(
    '[rad]',
    rotation='vertical',
    labelpad=labelpad,
)
ax.set_xlabel('time [s]')
ax.ticklabel_format(axis='y', style='sci', scilimits=(0, 0))
savefig_if_notexists(fig=fig, fpath='integration_approx_angular.png')

##################################################################
##################################################################
##################################################################
# COMPUTATION TIMES and ERROR TABLE
# labels = ['EXP-1', 'ME2-1', 'EXP-2', 'ME2-2']
labels = ['ref-1', 'traj-1', 'ref-2', 'traj-2']

logavg = None


def mot2np_lin(motionlist):
    return np.array([x.vector for x in motionlist], dtype=object)[:, :3]


def mot2np_ang(motionlist):
    return np.array([x.vector for x in motionlist], dtype=object)[:, 3:]


hist_err_ref = len(hist_err_me2) * [pin.Motion.Zero()]
hist_errs = [hist_err_ref, hist_err_me2, hist_err_ref, hist_err_me22]

tabular_rows = []
hist_compute_timesnp = np.array(hist_compute_times).T
for k, label in list(enumerate(labels)):
    label_compute_times = hist_compute_timesnp[k, :] * 1e6  # [s] to [us]
    nonzero_inds = label_compute_times.nonzero()
    label_compute_times = label_compute_times[nonzero_inds]
    compute_time_avg = np.average(label_compute_times)
    if 'ref' in label or 'EXP' in label:
        reference_label = True
    else:
        reference_label = False

    if reference_label:
        logavg = compute_time_avg
    compute_time_std = np.std(label_compute_times) * 1e3  # [us] to [ns]
    compute_time_max = np.max(label_compute_times)
    if logavg is None:
        sys.exit('logavg is None, check label names, where is log?')
    compute_time_relavg = (compute_time_avg / logavg) * 100
    print('avg time {}: {:.3g}us (rel: {:.2f}%)'.format(
        label, compute_time_avg, compute_time_relavg))
    compute_time_tabularstr = '{label}&&{avg:.2f}$\mu$s&{relavg:.1f}\\%&{max:.2f}$\mu$s&'.format(
        label=label,
        avg=compute_time_avg,
        max=compute_time_max,
        # std=compute_time_std,
        relavg=compute_time_relavg)
    # err stats
    label_errnp_linnorm = norm_from_vechist(mot2np_lin(
        hist_errs[k])) * 1e3  # [m] to [mm]
    err_lin_avg = np.average(label_errnp_linnorm)
    err_lin_std = np.std(label_errnp_linnorm)
    err_lin_max = np.max(label_errnp_linnorm)
    err_lin_tabularstr = '&&{avg:.2f}mm&{max:.2f}mm&$\pm${std:.2f}mm&'.format(
        avg=err_lin_avg, max=err_lin_max, std=err_lin_std)
    if reference_label:
        err_lin_tabularstr = '&&&$$'

    def rad2deg(rad):
        return rad * 180 / np.pi

    label_errnp_angnorm = norm_from_vechist(mot2np_ang(hist_errs[k]))
    err_ang_avg = np.average(label_errnp_angnorm)
    err_ang_std = np.std(label_errnp_angnorm)
    err_ang_max = np.max(label_errnp_angnorm)
    err_ang_tabularstr = '&&{avg:.2f}$\si{{\degree}}$&{max:.2f}$\si{{\degree}}$&$\pm${std:.2f}$\si{{\degree}}$&'.format(
        avg=rad2deg(err_ang_avg),
        max=rad2deg(err_ang_max),
        std=rad2deg(err_ang_std))
    if reference_label:
        err_ang_tabularstr = '&&&$$'

    tabular_rows.append(compute_time_tabularstr + err_lin_tabularstr +
                        err_ang_tabularstr + r'\\' + '\n')

savetabular_if_notexists(tabular_rows)

fig, ax = plt.subplots(1, 1, sharex='col')
lineObjects = ax.plot(tvec, hist_compute_times)
ax.legend(iter(lineObjects), labels, frameon=False)
ax.set_title('compute times')
# plt.close(fig)

# per-horizon computation time analysis
# for k, t_hor_end in enumerate(tsyncs[1:]):
#     t_hor_start = tsyncs[k]
#     horizon_times = hist_compute_timesnp[:, (tnp >= t_hor_start) &
#                                          (tnp < t_hor_end)]
#     # to remove zero computation times (which correspond to horizon sync time)
#     horizon_times[horizon_times == 0] = np.nan
#     horizon_compute_times_mean = np.nanmean(horizon_times, axis=1)
#     horizon_compute_times_std = np.nanstd(horizon_times, axis=1)
#     print('Horizon {} ({} points)'.format(k, horizon_times.shape[1]))
#     for kk, label in list(enumerate(labels)):
#         print('    avg time {}: {:.2f}us (std: {:.2f}us)'.format(
#             label, horizon_compute_times_mean[kk] * 1e6,
#             horizon_compute_times_std[kk] * 1e6))
##################################################################
##################################################################

plt.show()
