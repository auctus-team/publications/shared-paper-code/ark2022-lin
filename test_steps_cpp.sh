#!/usr/bin/bash

set -xe
yapf -i python/*.py
make -C build -j4
python -i python/test_steps_cpp.py
