# A linearization method based on Lie algebra for pose estimation in a time horizon
*by  Nicolas TORRES ALBERTO, Lucas JOSEPH, Vincent PADOIS and David DANEY*

*accepted at [ARK2022](https://ark2022.com/)*

## Abstract
 In this paper, a linearization strategy for robot pose estimation over a time horizon is presented. This linearization is crucial for the computationally efficient formulation of predictive and optimization-based control problems in robotics.
  The proposed approach is based on a truncation of the Magnus expansion and employs Lie algebra to represent position and orientation, allowing for a unified pose representation in vector form that can be integrated linearly over time, offering a convenient formulation for optimization solvers.
  The method shows promising results for precision and computation times.

<div align="center">
<img src="https://gitlab.inria.fr/auctus-team/publications/shared-paper-code/ark2022-lin/-/raw/master/images/integration_approx_angular.png" width="420px">
<img src="https://gitlab.inria.fr/auctus-team/publications/shared-paper-code/ark2022-lin/-/raw/master/images/integration_approx_linear.png" width="420px">
(orientation error on the left, position error on the right)

Results of using the proposed method (ME2) to estimate a robot's position and orientation subject to a perfectly known and variable velocity (twist) in a 300ms horizon, for two different trajectories. 

Refer to the paper for more details.
</div> 


  
## Content

This repository contains:

- Quickstart: The tl;dr of How to get the graphs on top?
- Development Environment: How to setup the development environment?
- Testing the method: How to reproduce the results in the paper?
  - with python
  - with python and C++ (needed for correct computation time measurements)

## Quickstart

If you're only interested in getting to the hacking stage as fast as possible, these steps will get the graphs on top:

```
git clone git@gitlab.inria.fr:auctus-team/publications/shared-paper-code/ark2022-lin.git
cd ark2022-lin/
conda env create -f python/environment.yml
conda activate ark22
python python/test_integration_approximation.py
```

**NOTE**: These steps are explained in more details in the following sections.


## Development Environment

### Clone repository
```
git clone git@gitlab.inria.fr:auctus-team/publications/shared-paper-code/ark2022-lin.git
cd ark2022-lin/
```

### Create the Conda environment

In order to obtain  cross-platform, easily reproducible and isolated development environment, [Conda](https://docs.conda.io/en/latest/) was used.

**NOTE**: This also includes the C++ dependencies.

Follow these steps to recreate the environment.

From inside repository root:
```
conda env create -f python/environment.yml
conda activate ark22
```

**NOTE**: If you're interested in setting up the environment from scratch, you ca check python/C++ dependencies in the `python/environment.yml`, which can be obtained from other sources (for instance, like `pip` and `apt**).

### Generating the normalized alpha profile

**This step is completely optional**.

The paper uses a normalized trapezoid velocity trajectory profile generated with [ruckig](https://github.com/pantor/ruckig). 

To avoid adding a dependency outside of conda (at the time of writting there's no available package in conda for ruckig), the alpha parameter is generated with `python/generate_alpha_ruckig.py` and saved to a python pickle file that will be used by the other scripts. **You do not need to do this, as the pickle file is already present in the repository**.

In order to manually generate this file, one needs to install ruckig with pip via: `pip install ruckig`.

## Testing the method

### Python

Once you have the development environment setup, you should be able to run:

```
conda activate ark22
python python/test_integration_approximation.py
```

### Python-C++ library

The paper refers to computation times. As the computation times are very small, we need to perform these operations in C++ to get accurate results. 
This is why there's a C++ library that will be called from python.

#### Compile the C++ library

Once you have the development environment setup, you should be able to run:

```
conda activate ark22
mkdir -pv build
cd build
cmake ../cpp
make -j
```

#### Execute the Python-C++ test
```
conda activate ark22
python python/test_integration_approximation_cpp.py
```

The same procedure can be used for:
```
conda activate ark22
python python/test_steps_cpp.py
```
