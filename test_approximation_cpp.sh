#!/usr/bin/bash

set -xe
yapf -i python/*.py
make -C build -j4
python -i python/test_integration_approximation_cpp.py
