#include <Eigen/Core>
#include <Eigen/Dense>
#include <chrono>
#include <tuple>
#include <vector>
// #include <iostream>

#include <pybind11/eigen.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include <pinocchio/spatial/explog.hpp>
#include <pinocchio/spatial/se3.hpp>
#include <pinocchio/spatial/skew.hpp>

#define STRINGIFY(x) #x
#define MACRO_STRINGIFY(x) STRINGIFY(x)

using Vector6d = Eigen::Matrix<double, 6, 1>;
using SquareMatrix6d = Eigen::Matrix<double, 6, 6>;
using RefVector6d = const Eigen::Ref<Vector6d>;

struct ResTime {
  ResTime() : result(Vector6d::Zero()) {}
  void tick() { startt = std::chrono::steady_clock::now(); }
  void tock() {
    endd = std::chrono::steady_clock::now();
    total_ns =
        std::chrono::duration_cast<std::chrono::nanoseconds>(endd - startt)
            .count();
  }

  double total_ns;
  std::chrono::time_point<std::chrono::steady_clock> startt;
  std::chrono::time_point<std::chrono::steady_clock> endd;
  Vector6d result;
};

Vector6d brse3(const Vector6d &x, const Vector6d &y) {
  const auto &lin1 = x.head<3>();
  const auto &ang1 = x.tail<3>();
  const auto &lin2 = y.head<3>();
  const auto &ang2 = y.tail<3>();

  Vector6d res;
  res << ang1.cross(lin2) - ang2.cross(lin1), ang1.cross(ang2);
  return res;
}

SquareMatrix6d dexp(const Vector6d &x) {
  const auto &lin = x.head<3>();
  const auto &ang = x.tail<3>();
  SquareMatrix6d dexp0(SquareMatrix6d::Zero());
  pinocchio::skew(ang, dexp0.block<3, 3>(0, 0));
  pinocchio::skew(lin, dexp0.block<3, 3>(0, 3));
  // pinocchio::skew(ang, dexp0.block<3, 3>(3, 3));
  dexp0.block<3, 3>(3, 3) = dexp0.block<3, 3>(0, 0);
  return SquareMatrix6d::Identity() + dexp0 / 2 + dexp0 * dexp0 / 12;
}

ResTime pinlog_timed(const Vector6d &x, const Vector6d &y) {
  pinocchio::SE3 T1 = pinocchio ::exp6(x);
  pinocchio::SE3 T2 = pinocchio ::exp6(y);

  ResTime restime;
  restime.tick();
  restime.result = pinocchio::log6(T1.act(T2)).toVector();
  restime.tock();
  return restime;
}

// ************************************************************************
// NOTE: these 2 implementations should be equivalent but it seems the
// commented one is slower.
Vector6d me2(const Vector6d &x0, const Vector6d &x, const Vector6d &dx) {
  auto br1 = brse3(x0, dx);
  auto br2 = brse3(x0, br1);

  return x + dx + br1 / 2 // n=1
         + br2 / 12;      // n=2
}

// Vector6d me2(const Vector6d &x0, const Vector6d &x, const Vector6d &dx) {
//   return x + dexp(x0) * dx;
// }
// ************************************************************************

ResTime me2_timed(RefVector6d x0, RefVector6d x, RefVector6d dx) {
  ResTime restime;
  restime.tick();
  restime.result = me2(x0, x, dx);
  restime.tock();
  return restime;
}

namespace py = pybind11;

PYBIND11_MODULE(lin, m) {
  py::class_<ResTime>(m, "ResTime")
      .def(py::init<>())
      .def_readonly("total_ns", &ResTime::total_ns)
      .def_readwrite("result", &ResTime::result);

  m.doc() = R"pbdoc(
           This library is used to efficiently compute the truncation-based expansion
           of the inverse of the dexp map from python.
           reference: https://gitlab.inria.fr/auctus-team/publications/shared-paper-code/ark2022-lin
    )pbdoc";

  m.def("me2", &me2, R"pbdoc(
        Use the truncation-based Magnus Expansion of dexp inverse.
    )pbdoc");

  m.def("me2_timed", &me2_timed, R"pbdoc(
        Use the truncation-based Magnus Expansion of dexp inverse.
    )pbdoc");

  m.def("pinlog_timed", &pinlog_timed, R"pbdoc(
        pinocchio::log
    )pbdoc");

#ifdef VERSION_INFO
  m.attr("__version__") = MACRO_STRINGIFY(VERSION_INFO);
#else
  m.attr("__version__") = "dev";
#endif
}
