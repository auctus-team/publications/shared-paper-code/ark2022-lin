#!/usr/bin/bash

set -xe
yapf -i python/*.py
python -i python/test_integration_approximation.py
