#!/usr/bin/bash

set -xe
git commit --amend --no-edit . 
git push -f
